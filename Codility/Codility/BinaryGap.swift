//
//  BinaryGap.swift
//  Codility
//
//  Created by Lopez, Norman (ETW - WHQ) on 2/1/16.
//  Copyright © 2016 Norman. All rights reserved.
//

import Foundation

public func convertDigitToBin(n : Int) -> String {
    
    var result = ""
    var tempn = n
    
    func incrementZero() {
        result += "0"
    }
    func incrementOne() {
        result += "1"
    }
    
    func trimZerostoLeft() -> String {
        
        var r = ""
        var flag = false
        for char in result.characters {
            if (flag || char == "1" ){
                flag = true
                r.append(char)
            }
        }
//        print("my binary digit: \(r)")
        return r
    }
    
    
    for var index = 31; index >= 0; index-- {
        let k = 1 << index //Int((pow(2.0, Double(index))))
        (tempn >> index != 0) ? incrementOne() : incrementZero()
        tempn = tempn % k
    }
    return trimZerostoLeft()
}


func solveBinaryGap_number_of_gaps(str : String) -> Int{
    
    var countGaps = 0
    var sw_zeros = false
    var index : Int = 0
    
    for char in str.characters {
        if(index > 0 ){
            
            let prev = str.startIndex.advancedBy(index-1)
            if (char == "0" && str[prev] == "1") {
                sw_zeros = true
            }
            
            if (sw_zeros && char == "1"){
                sw_zeros = false
                countGaps += 1
            }
        }
        index++
    }
    return countGaps
}

func solveBinaryGap_longest(str : String) -> Int{
    
    var sw_zeros = false
    var index : Int = 0
    var maxGap = 0
    
    var count_gap_zeros=0
    for char in str.characters {
        if(index > 0 ){
            
            if (sw_zeros && char == "1"){
                if (count_gap_zeros > maxGap){
                    maxGap = count_gap_zeros
                }
                count_gap_zeros = 0
                sw_zeros = false
            }
            
            if(sw_zeros){
                count_gap_zeros+=1
            }
            
            let prev = str.startIndex.advancedBy(index-1)
            if (char == "0" && str[prev] == "1") {
                count_gap_zeros+=1
                sw_zeros = true
            }
            
        }
        index++
    }
    return maxGap
}

public func solution(N : Int) -> Int {
    // write your code in Swift 2.2
    let binary = convertDigitToBin(N)
    
//    print("binary: \(convertDigitToBin(N))")
    return solveBinaryGap_longest(binary)
}