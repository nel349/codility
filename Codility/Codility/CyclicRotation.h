//
//  CyclicRotation.h
//  Codility
//
//  Created by Lopez, Norman (ETW - WHQ) on 2/4/16.
//  Copyright © 2016 Norman. All rights reserved.
//

#ifndef CyclicRotation_h
#define CyclicRotation_h
@import Foundation;

@interface CyclicRotation : NSObject

+(NSMutableArray *) solution : (NSMutableArray *) A : (int) K;

@end

#endif /* CyclicRotation_h */
