//
//  CyclicRotation.m
//  Codility
//
//  Created by Lopez, Norman (ETW - WHQ) on 2/4/16.
//  Copyright © 2016 Norman. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CyclicRotation.h"

@implementation CyclicRotation

+ (NSMutableArray *) solution : (NSMutableArray *) A : (int) K  {
    
    if (![A count] || K == 0){ // Check if array is empty
        return A;
    }
    
    for( int j = 0; j < K ; j++){
        NSUInteger length = [A count];
        NSUInteger last = [A count] - 1;
        NSNumber *tempLast = A[last];
        for (NSUInteger i = length - 1; i > 0; --i) {
            
            //        NSLog(@"%@", A[i]);
            A[i] = A[i-1];
        }
        
        A[0]= tempLast;
        
    }
    return A;

}

@end