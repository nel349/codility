//
//  main.swift
//  Codility
//
//  Created by Lopez, Norman (ETW - WHQ) on 2/1/16.
//  Copyright © 2016 Norman. All rights reserved.
//

import Foundation


func BinaryGap(){
    //print("Hello, World!")
    //
    //print(convertDigitToBin(7))
    //assert(convertDigitToBin(647) == "1010000111", "false")
    //assert(convertDigitToBin(5) == "101", "false")
    //assert(convertDigitToBin(4) == "100", "false")
    //assert(convertDigitToBin(11) == "1011", "false")
    //assert(convertDigitToBin(15) == "1111", "false")
    
    //print("Number of binary gap: \( solveBinaryGap("1010101") )")
    assert(solveBinaryGap_number_of_gaps("1010101") == 3)
    assert(solveBinaryGap_number_of_gaps("10101010") == 3)
    assert(solveBinaryGap_number_of_gaps("10101") == 2)
    assert(solveBinaryGap_number_of_gaps("1010") == 1)
    assert(solveBinaryGap_number_of_gaps("100001000010001010111100001") == 6)
    var actual = solution(1041)
    var expected = 5
    assert(actual == expected , "\(actual) should be \(expected)")
    
    actual = solution(69277153)
    expected = 4
    assert(actual == expected , "\(actual) should be \(expected)")
    
    
    
    actual = solution(16)
    expected = 0
    assert(actual == expected , "\(actual) should be \(expected)")
    
    actual = solution(1024)
    expected = 0
    assert(actual == expected , "\(actual) should be \(expected)")
}


func runCyclicRotation () {
    var array : [Int] = [1, 2, 4, 5]
    var arrayMutable : NSMutableArray = [1, 2, 3, 4]
//    array.append(2)
//    var array2 = [Int]()
    
//    print(arrayMutable)
    
    
    var actual = CyclicRotation.solution(arrayMutable, 2)
    var expected = [3,4,1,2];
    assert(actual == expected)
    
    
    //Test1
    arrayMutable = [3,8,9,7,6]
    actual = CyclicRotation.solution(arrayMutable, 3)
    expected = [9,7,6,3,8];
    assert(actual == expected)
    
    
    //Test2
    arrayMutable = []
    actual = CyclicRotation.solution(arrayMutable, 3)
    expected = [];
    assert(actual == expected)
    
    //Test2
    arrayMutable = [1]
    actual = CyclicRotation.solution(arrayMutable, 3)
    expected = [1];
    assert(actual == expected)
    
    //Test3
    arrayMutable = [1, 2]
    actual = CyclicRotation.solution(arrayMutable, 3)
    expected = [2, 1];
    assert(actual == expected)
    
    //Test4
    arrayMutable = [3,8,9,7,6];
    actual = CyclicRotation.solution(arrayMutable, 0)
    expected = [3,8,9,7,6];
    assert(actual == expected)
    
    
    
}

func OddOcurrencesInArray(){

    var arrayMutable = [1, 2, 2, 3, 3]
    
    var actual = solution(&arrayMutable)
    var expected = 1
    assert(actual == expected)
    
    //Test1
    arrayMutable = [1,1, 2, 2, 3, 3, 4]
    
    actual = solution(&arrayMutable)
    expected = 4
    assert(actual == expected)
    
    //Test1
    arrayMutable = [3]
    
    actual = solution(&arrayMutable)
    expected = 3
    assert(actual == expected)
}


OddOcurrencesInArray()
