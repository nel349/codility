//
//  L2_OddOcurrencesInArray.swift
//  Codility
//
//  Created by Lopez, Norman (ETW - WHQ) on 2/5/16.
//  Copyright © 2016 Norman. All rights reserved.
//

import Foundation


public func solution(inout A : [Int]) -> Int {
    
    var occurrences = [Int: Int]()
    
    for key in A {
        if occurrences[key] != nil {
            occurrences.removeValueForKey(key)
        }
        else {
            occurrences[key] = 1
        }
        
        
        
        //        print("key: \(key) , value: \(occurrences[key]!)")
    }
    
    for key in occurrences.keys {
        if occurrences[key] != nil && occurrences[key]! % 2 != 0 {
            return key
        }
    }
    
    return 0
}